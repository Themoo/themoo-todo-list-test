package com.nachiengmai.moo.todolisttest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;


public class EditActivity extends ActionBarActivity {
    private TodoList editTodoList;
    EditText editTodoText;
    Switch todoSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        editTodoList = (TodoList) getIntent().getSerializableExtra("editTask");
        editTodoText = (EditText)findViewById(R.id.edit_editText);
        editTodoText.setText(editTodoList.getTodoText());
        todoSwitch = (Switch)findViewById(R.id.doit_switch);
        if (editTodoList.getDoIt()==0)
            todoSwitch.setChecked(false);
        else
            todoSwitch.setChecked(true);

        todoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TodoListDAO todoListDAO = new TodoListDAO(getApplicationContext());
                todoListDAO.open();
                if (isChecked) {
                    todoListDAO.doIt(editTodoList, true);
                    Log.d("DoIt", "True");
                }
                else {
                    todoListDAO.doIt(editTodoList, false);
                    Log.d("DoIt", "False");
                }
                todoListDAO.close();
            }
        });

        Button button = (Button)findViewById(R.id.edit_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTodoList.setTodoText(String.valueOf(editTodoText.getText()));
                TodoListDAO todoListDAO = new TodoListDAO(getApplicationContext());
                todoListDAO.open();
                todoListDAO.update(editTodoList);
                todoListDAO.close();
                Toast.makeText(getApplicationContext(),"Update OK ;)",Toast.LENGTH_SHORT).show();
            }
        });

        Button delBtn = (Button)findViewById(R.id.del_button);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditActivity.this);
                alertDialog.setTitle("Confirm???");
                alertDialog.setMessage("Delete OK?");
                alertDialog.setNegativeButton("Cancel", null);
                alertDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Toast.makeText(getApplicationContext(), "DELETE!!!", Toast.LENGTH_SHORT).show();
                        TodoListDAO todoListDAO = new TodoListDAO(getApplicationContext());
                        todoListDAO.open();
                        todoListDAO.delete(editTodoList);
                        todoListDAO.close();
                        finish();
                    }
                });
                alertDialog.show();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
