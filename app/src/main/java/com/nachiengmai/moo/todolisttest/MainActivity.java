package com.nachiengmai.moo.todolisttest;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;


public class MainActivity extends ActionBarActivity {
    ListView myList;
    Button textBtn, allBtn;
    EditText textEditText;
    private TodoListview adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myList = (ListView)findViewById(R.id.todo_listView);
        this.updateListView();

        textBtn = (Button)findViewById(R.id.text_button);
        allBtn = (Button)findViewById(R.id.all_button);
        allBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateListView();
            }
        });

        textEditText = (EditText)findViewById(R.id.search_editText);

        textBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TodoListDAO todoListDAO = new TodoListDAO(getApplicationContext());
                todoListDAO.open();
                adapter = new TodoListview(MainActivity.this, todoListDAO.findByText(String.valueOf(textEditText.getText())));
                todoListDAO.close();
                myList.setAdapter(adapter);
            }
        });



    }

    private void updateListView() {
        TodoListDAO todoListDAO = new TodoListDAO(this);
        todoListDAO.open();

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,android.R.id.text1,
                todoListDAO.getAllTodoList());*/
        adapter = new TodoListview(this, todoListDAO.getAllTodoList());

        myList.setAdapter(adapter);
        todoListDAO.close();

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), String.valueOf(adapter.getItem(position).getId()),Toast.LENGTH_SHORT).show();
                Intent editIntent = new Intent(getApplicationContext(), EditActivity.class);
                editIntent.putExtra("editTask", adapter.getItem(position));
                startActivity(editIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.updateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            Intent newIntent = new Intent(this, ActivityAddTodoList.class);
            startActivity(newIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
