package com.nachiengmai.moo.todolisttest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 7/1/15 AD.
 */
public class TodoListDAO {
    private SQLiteDatabase database;
    private DbHelper dbHelper;
    private String[] allCollumn = {"id", "todo_text", "do_it"};

    public TodoListDAO(Context context) {
        dbHelper = new DbHelper(context);
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        dbHelper.close();
    }

    public ArrayList<TodoList> getAllTodoList(){
        ArrayList<TodoList> todoList = new ArrayList<TodoList>();
        Cursor cursor = database.query("todo_list", allCollumn,null,null,null,null,null);
        cursor.moveToFirst();
        TodoList todoList1;
        while (!cursor.isAfterLast()){
            todoList1 = new TodoList();
            todoList1.setId(cursor.getInt(0));
            todoList1.setTodoText(cursor.getString(1));
            todoList1.setDoIt(cursor.getInt(2));
            todoList.add(todoList1);
            cursor.moveToNext();
        }
        cursor.close();
        return todoList;
    }

    public ArrayList<TodoList> findByText(String searchText){
        ArrayList<TodoList> todoListsArray = new ArrayList<TodoList>();
        String SQLText = "SELECT * FROM todo_list WHERE todo_text LIKE '%" + searchText + "%';";
        Cursor cursor = this.database.rawQuery(SQLText, null);
        cursor.moveToFirst();
        TodoList todoList;
        while (!cursor.isAfterLast()){
            todoList = new TodoList();
            todoList.setId(cursor.getInt(0));
            todoList.setTodoText(cursor.getString(1));
            todoList.setDoIt(cursor.getInt(2));
            todoListsArray.add(todoList);
            cursor.moveToNext();
        }
        cursor.close();
        return  todoListsArray;
    }

    public void add(TodoList todoList) {
        TodoList newTodoList = new TodoList();
        newTodoList = todoList;

        ContentValues values = new ContentValues();
        values.put("todo_text", newTodoList.getTodoText());
        values.put("do_it", 0);
        this.database.insert("todo_list", null, values);
        Log.d("Mooooooo:", "Add OK ;)");
    }

    public void update(TodoList todoList){
        TodoList editTodoList = todoList;

        ContentValues values = new ContentValues();
        values.put("todo_text", editTodoList.getTodoText());
        String where = "id=" + editTodoList.getId();
        this.database.update("todo_list", values, where, null);
        Log.d("Mooooo::::", "Update OK ;)");
    }

    public void delete(TodoList todoList){
        TodoList delTodoList = todoList;
        Log.d("Mooooo:::", String.valueOf(delTodoList.getId()));
        String delSQLText = "DELETE FROM todo_list where id=" + delTodoList.getId();

        this.database.execSQL(delSQLText);
        Log.d("Mooooo:::", "Delete OK ;)");
    }

    public void doIt(TodoList todoList, Boolean doIt){
        TodoList doItTodoList = todoList;
        int doItInt = 0;
        if (doIt)
            doItInt = 1;
        String sqlText = "UPDATE todo_list SET do_it=" + doItInt +
                " WHERE id=" + doItTodoList.getId() + ";";
        this.database.execSQL(sqlText);
        Log.d("Mooooo:::", "Change do_it OK");
    }
}
