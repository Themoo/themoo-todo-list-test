package com.nachiengmai.moo.todolisttest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 7/8/15 AD.
 */
public class TodoListview extends BaseAdapter {
    private static Activity activity;
    private static LayoutInflater inflater;
    ArrayList<TodoList> arrayList;

    public TodoListview(Activity activity, ArrayList<TodoList> arrayList) {
        super();
        this.activity = activity;
        this.arrayList = arrayList;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public TodoList getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (convertView == null)
            v = inflater.inflate(R.layout.todo_list_layout, null);
        TextView textView = (TextView)v.findViewById(R.id.task_text);
        TodoList todoList = arrayList.get(position);
        textView.setText(todoList.getTodoText());
        LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.todo_list_background);
        if (todoList.getDoIt() == 1)
            linearLayout.setBackgroundColor(Color.GREEN);
        return v;
    }
}
