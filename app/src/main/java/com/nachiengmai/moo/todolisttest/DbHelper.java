package com.nachiengmai.moo.todolisttest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by wacharapongnachiengmai on 7/1/15 AD.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String databaseName = "todolist.db";
    private static final int databaseVersion = 2;
    private static final String tableCreateSQL = "CREATE TABLE todo_list (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "todo_text TEXT" +
            ");";
    Context myContext;

    public DbHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        myContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tableCreateSQL);
        String insertData1 = "INSERT INTO todo_list (todo_text) VALUES ('Todo Text 1');";
        String insertData2 = "INSERT INTO todo_list (todo_text) VALUES ('Todo Text 2');";
        String insertData3 = "INSERT INTO todo_list (todo_text) VALUES ('Todo Text 3');";
        String insertData4 = "INSERT INTO todo_list (todo_text) VALUES ('Todo Text 4');";
        String insertData5 = "INSERT INTO todo_list (todo_text) VALUES ('Todo Text 5');";
        db.execSQL(insertData1);
        db.execSQL(insertData2);
        db.execSQL(insertData3);
        db.execSQL(insertData4);
        db.execSQL(insertData5);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sqlText = "ALTER TABLE todo_list " +
                "ADD COLUMN do_it INTEGER;";
        db.execSQL(sqlText);
        sqlText = "UPDATE todo_list set do_it=0;";
        db.execSQL(sqlText);
    }
}
