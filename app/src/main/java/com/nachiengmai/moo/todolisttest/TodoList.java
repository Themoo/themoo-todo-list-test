package com.nachiengmai.moo.todolisttest;

import android.text.Editable;

import java.io.Serializable;

/**
 * Created by wacharapongnachiengmai on 7/1/15 AD.
 */
public class TodoList implements Serializable {
    private int id;
    private String TodoText;
    private int doIt;

    public int getDoIt() {
        return doIt;
    }

    public void setDoIt(int doIt) {
        this.doIt = doIt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTodoText() {
        return TodoText;
    }

    public void setTodoText(String todoText) {
        TodoText = todoText;
    }

    @Override
    public String toString() {
        return "TodoList{" +
                "id=" + id +
                ", TodoText='" + TodoText + '\'' +
                '}';
    }
}
